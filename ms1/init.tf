provider "aws" {
    region      = "${var.region}" 
    profile     = "${var.profile}"
    shared_credentials_file  = "${var.creds_file}"
}

terraform {
  backend "remote" {
    hostname = "app.terraform.io"
    organization = "elucidatacorp"

    workspaces {
      name = "micro-service1"
    }
  }
}
