variable "region" {
    description = "Holds the value of AWS Resource Region"
}

variable "creds_file" {
    description = "Holds the path of AWS credentials file"
}

variable "profile" {
    description = "Holds the value of AWS profile to be used" 
}
