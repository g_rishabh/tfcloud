provider "aws" {
    region      = "${var.region}" 
    profile     = "${var.profile}"
    shared_credentials_file  = "${var.creds_file}"
}

terraform {
  backend "s3" {
    bucket                  = "mithoo-dev-tf-state"
    key                     = "codebuild/cbuild.tfstate"
  }
}
