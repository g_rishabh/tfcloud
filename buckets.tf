resource "aws_s3_bucket" "test-bucket" {
  bucket = "tfcloud-test"
  acl    = "private"

  tags = {
    Environment = "dev"
  }
}

resource "aws_s3_bucket" "test-bucket-2" {
  bucket = "tfcloud-test2"
  acl    = "private"

  tags = {
    Environment = "dev"
  }
}

resource "aws_s3_bucket" "test-bucket-3" {
  bucket = "tfcloud-test2222"
  acl    = "private"

  tags = {
    Environment = "dev"
  }
}


